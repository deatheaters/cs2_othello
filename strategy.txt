Want:
	Decision tree
	alpha-beta pruning
	don't search through all squares to find possible moves
	transposition table
	recursing as far as we can within time limit
	tuning our scoring function
	tune scoring function to include game length (early game, midgame, endgame)
	opening book
	
Decision tree:
	need to write/use a tree implementation
	need to write a recursive function that given a recursion limit
	calculates all possible moves to that recursion limit and their scores
	
Alpha-beta pruning:
	implement alpha beta pruning in our decision tree

Possible moves searching:
	currently search through all 64 squares to find possible moves - not good
	write better way to find possible moves
	might need to write a new board implementation
	thinking a 64 length vector (with say index = height * 8 + width)
	Or a 64 length vector with each piece filling the vector
	
Transposition table:
	Basically memoization for the score function
	need to hash the board state look at Zobrist hashing
	need to write a hash map to store the board state

Recursing as far as we can:
	need to keep track of time so that recurse as many levels as we can
	and then have enough time to determine the best move and return that move

Tuning our scoring function:
	Find smart ways to determine how good a current board state is
	Not just weight corners more, what about mobility or other things
	probably will require a good amount of empirical determination

Tune scoring function for game length:
	determine whether we are in the early, mid or late game and make
	the weight function take this info into account

Opening book:
	write a list of good opening for othello and a way for the program to read
	them at runtime


