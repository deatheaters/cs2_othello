from subprocess import Popen
from subprocess import PIPE
import time

AI_1_name = 'Death_Eaters'
AI_2_name = 'ConstantTimePlayer'
time_per_game = 5
num_games = 50

command = './testgame ' + AI_1_name + ' ' + AI_2_name

num_black_wins = 0
process_list = []

results = []

for i in range(num_games):
    process_list.append(Popen(command, shell=True, stdout=PIPE))

time.sleep(num_games)

for proc in process_list:
    (out, _) = proc.communicate()
    results.append(out)

for string in results:
    if string[-11] == 'B':
        num_black_wins += 1

print (float(num_black_wins) / num_games)
