#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    own_team = side;
    opp_team = opposite_team(side);
    board = new Board();
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete board;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    const int RECURSION_LIMIT = 8;
    // If the opponent has made a move, make it
    if (opponentsMove != NULL) {
	// update the board to reflect the move
	board->doMove(opponentsMove, opp_team);
    }
    if (!board->hasMoves(own_team)) {
	return NULL;
    }
    scored_move * chosen_move = best_move(board, RECURSION_LIMIT, 
					  own_team, -9999999, 9999999);
    Move * our_move = chosen_move->move_made;
    // BLATANT HACK
    // for some reason our code, sometimes doesn't work at the end
    // @Liz, you should make a real fix/investigate why the best_move code doesn't
    // work well in general
    if (our_move == NULL) {
	std::list<Move*> possible_moves;
	for (int i = 0; i < 8; i++) {
	    for (int j = 0; j < 8; j++) {
		Move * test_move = new Move(i, j);
		if (board->checkMove(test_move, own_team)) {
		    // valid move; add it to the list
		    possible_moves.push_front(test_move);
		}
		else {
		    delete test_move;
		}
	    }
	}
	our_move = possible_moves.front();
    }
	
    // make the move
    // this is important because otherwise the board isn't kept current
    board->doMove(our_move, own_team);
	// add this to a list of moves we've made
    return our_move;
}


int ExamplePlayer::calculate_score(Board * test_board, Side team) {
    return test_board->calculateScore(team);
}

scored_move * ExamplePlayer::best_move(Board * curr_board, int iteration_depth, Side team, int alpha, int beta) {
    std::list<Move*> possible_moves;
    std::list<Move*>::iterator it;
    Move * our_move = NULL;
    scored_move * evaluated_move = new scored_move;
    int highest_score = -100;
	/*int OURmobility = 0;
	int OPPmobility = 0; 
	*/
	
    if (iteration_depth <= 0) {
	highest_score = calculate_score(curr_board, team);
	// return 
    }
    else {
	for (int i = 0; i < 8; i++) {
	    for (int j = 0; j < 8; j++) {
		Move * test_move = new Move(i, j);
		if (curr_board->checkMove(test_move, team)) {
		    // valid move; add it to the list
		    possible_moves.push_front(test_move);
			// OURmobility += 1;
		}
		/* 
		if (curr_board->checkMove(test_move, opposite_team(team))) {
			OPPmobility += 1;
		}
		*/
		else {
		    delete test_move;
		}
	    }
	}

	if (team == own_team) {
	    for (it = possible_moves.begin(); it == possible_moves.end(); ++it) { 
		Board * test_board = board->copy();
		Move * test_move = *it;
		test_board->doMove(test_move, team); //change the test board
		if (test_board->hasMoves(opposite_team(team))) {
		    evaluated_move = best_move(test_board, 
					       (iteration_depth - 1), 
					       opposite_team(team), alpha, beta);
		    if (evaluated_move->score > highest_score) {
			our_move = test_move;
			highest_score = evaluated_move->score;
			alpha = evaluated_move->score;
		    }
		}
		else {
		    if (test_board->stoneDifference(team) > 0) {
			highest_score = 9999999;
			alpha = 999999;
		    }
		    else if (test_board->stoneDifference(team) == 0) {
			highest_score = 0;
			alpha = 0;
		    }
		    else {
			highest_score = -999999;
			alpha = -999999;
		    }
		}
		if (beta <= alpha) {
		    break;
		}
	    }
	}
	else {
	    for (it = possible_moves.begin(); it == possible_moves.end(); ++it) { 
		Board * test_board = board->copy();
		Move * test_move = *it;
		test_board->doMove(test_move, team);
		if (test_board->hasMoves(team)) { //!! before just team - changed back
		    evaluated_move = best_move(test_board, 
					       (iteration_depth - 1), 
					       opposite_team(team), alpha, beta);
		    if (evaluated_move->score > highest_score) {
			our_move = test_move;
			highest_score = evaluated_move->score;
			beta = evaluated_move->score;
		    }
		}
		else {
		    if (test_board->stoneDifference(team) > 0) {
			highest_score = -9999999;
			alpha = -999999;
		    }
		    else if (test_board->stoneDifference(team) == 0) {
			highest_score = 0;
			alpha = 0;
		    }
		    else {
			highest_score = 999999;
			alpha = 999999;
		    }
		}
		if (beta <= alpha) {
		    break;
		}
	    }
	}
    }
    scored_move * high_score_move = new scored_move;
    high_score_move->score = highest_score;
    high_score_move->move_made = our_move;
    return high_score_move;
}

Side ExamplePlayer::opposite_team(Side team) {
    Side opposing_team;
    if (team == (Side) WHITE) {
	opposing_team = (Side) BLACK;
    }
    else {
	opposing_team = (Side) WHITE;
    }
    return opposing_team;
}
