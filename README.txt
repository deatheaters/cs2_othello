David Flicker did the following:
	1. Implemented basic functionality for last week such as making correct 	moves and a basic, one move-ahead heuristic
	2. Implemented Minimax
	3. Implemented alpha-beta pruning
	4. Wrote a python script to automate testing of the AI
	5. Rewrote the heuristic for minimax

Elizabeth Lawler did the following:
	1. worked on debugging minimax and alpha-beta pruning
	2. worked on tuning the heuristic
	3. started working on adding weight to moves that have greater mobility and less exposed squares (didn't finish).

Improvements made:
	Added minimax so that our AI can think ahead.
	Added alpha-beta pruning so that it can recurse even further ahead
	Made an improved heuristic so that the AI evaluates board position better
	
Improvements that aren't working as well as they should:
	Minimax and the general recursive move scoring algorithms aren't working as 
	well as they should. I'm not sure if its due to a bad heuristic or bad
	alpha-beta pruning.
	Also, at the end game, sometimes the recursive move analyzer sometimes
	returns a null move even though there is a move availible.
	
