#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <list>

using namespace std;

typedef struct ScoredMove {
    int score;
    Move * move_made;
    int alpha;
    int beta;
} scored_move;

class ExamplePlayer {


 private:
    Side own_team;
    Side opp_team;
    Board* board;
    int calculate_score(Board * curr_board, Side team);
 
    scored_move * best_move(Board * curr_board, int iteration_depth, Side team, int alpha, int beta);
    Side opposite_team(Side team);

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
